# TITRE Ier

# Dispositions tendant à l’amélioration de l’équilibre des relations commerciales dans le secteur agricole et alimentaire

## Article 1er

I. – Les articles L. 631‑24 à L. 631‑24‑2 du code rural et de la pêche maritime sont ainsi modifiés :

1° Les articles L. 631‑24‑1 et L. 631‑24‑2 deviennent les articles L. 631‑24‑4 et L. 631‑24‑5 ;

2° L’article L. 631‑24 est remplacé par quatre articles ainsi rédigés :

« Art. L. 631‑24.  – Tout contrat de vente de produits agricoles livrés sur le territoire français est régi, lorsqu’il est conclu sous forme écrite, par les dispositions du présent article.

« I. – La conclusion d’un contrat de vente écrit relatif à la cession à leur premier acheteur de produits agricoles figurant à l’annexe I du règlement (UE) n° 1308/2013 du Parlement européen et du Conseil du 17 décembre 2013 portant organisation commune des marchés des produits agricoles, destinés à la revente ou à la transformation en vue de la revente est précédée d’une proposition du producteur agricole, sous réserve, dans les cas où la conclusion d’un contrat écrit n’est pas obligatoire, des dispositions du paragraphe 1 bis des articles 148 et 168 de ce règlement.

« Lorsque le producteur a donné mandat à une organisation de producteurs reconnue dont il est membre, ou à une association d’organisations de producteurs reconnue à laquelle appartient l’organisation de producteurs dont il est membre, pour négocier la commercialisation de ses produits, sans qu’il y ait transfert de leur propriété, la conclusion par lui d’un contrat écrit avec un acheteur pour la vente des produits en cause est subordonnée au respect des stipulations de l’accord‑cadre écrit conclu avec l’acheteur par cette organisation de producteurs ou association d’organisations de producteurs. L’organisation de producteurs ou l’association d’organisations de producteurs propose à l’acheteur un accord‑cadre écrit conforme aux prescriptions du présent article.

« II. – La proposition de contrat ou d’accord‑cadre écrit mentionnée au I et le contrat ou l’accord‑cadre écrit conclu comportent des clauses relatives :

« 1° Au prix ou aux critères et modalités de détermination et de révision du prix ;

« 2° Aux volumes et aux caractéristiques des produits qui peuvent ou doivent être livrés ;

« 3° Aux modalités de collecte ou de livraison des produits ;

« 4° Aux modalités de paiement ;

« 5° À la durée du contrat ;

« 6° Aux règles applicables en cas de force majeure ;

« 7° Aux délai de préavis et indemnité éventuellement applicables dans les différents cas de résiliation du contrat, notamment dans l’hypothèse où la résiliation est motivée par une modification du mode de production.

« Les critères et modalités de détermination du prix mentionnés au 1° prennent en compte un ou plusieurs indicateurs relatifs aux coûts pertinents de production en agriculture ou à l’évolution de ces coûts, un ou plusieurs indicateurs relatifs aux prix des produits agricoles et alimentaires constatés sur le ou les marchés sur lesquels opère l’acheteur, ainsi que, le cas échéant, un ou plusieurs indicateurs relatifs aux quantités, à la composition, à la qualité, à la traçabilité, ou au respect d’un cahier des charges. Les parties peuvent utiliser tous indicateurs disponibles ou spécialement construits par elles.

« Les contrats, accords‑cadres et propositions de contrats et accords‑cadres mentionnés au premier alinéa comportent également, le cas échéant, la clause mentionnée à l’article L. 441‑8 du code de commerce.

« III. – La proposition d’accord‑cadre écrit et l’accord‑cadre conclu mentionnés au premier alinéa du II précisent :

« 1° La quantité totale et la qualité à livrer par les producteurs membres de l’organisation ou les producteurs représentés par l’association ;

« 2° La répartition des quantités à livrer entre les producteurs membres de l’organisation ou les producteurs représentés par l’association et les modalités de cession des contrats ;

« 3° Les modalités de gestion des écarts entre le volume ou la quantité à livrer et le volume ou la quantité effectivement livré par les producteurs membres de l’organisation ou les producteurs représentés par l’association ;

« 4° Les règles organisant les relations entre l’acheteur et l’organisation de producteurs ou l’association d’organisations de producteurs, notamment les modalités de la négociation périodique sur les volumes et le prix ou les modalités de détermination du prix entre l’acheteur et l’organisation de producteurs ou l’association d’organisations de producteurs.

« L’acheteur transmet chaque mois à l’organisation de producteurs ou à l’association d’organisations de producteurs avec laquelle un accord‑cadre a été conclu les éléments figurant sur les factures individuelles des producteurs membres ayant donné un mandat de facturation à l’acheteur et les critères et modalités de détermination du prix d’achat aux producteurs. Les modalités de transmission de ces informations sont précisées dans un document écrit.

« IV. – Dans le cas où l’établissement de la facturation par le producteur est délégué à un tiers ou à l’acheteur, il fait l’objet d’un mandat écrit distinct du contrat.

« Le mandat de facturation est renouvelé chaque année par tacite reconduction.

« Le producteur peut révoquer ce mandat à tout moment, sous réserve d’un préavis d’un mois.

« V. – Le contrat écrit ou l’accord‑cadre écrit est renouvelable par tacite reconduction pour une période équivalente à celle pour laquelle il a été conclu, sauf stipulations contraires. Il fixe le préavis applicable au cas où il ne serait pas renouvelé.

« Art. L. 631‑24‑1. – Lorsque l’acheteur revend des produits agricoles ou des produits alimentaires comportant un ou plusieurs produits agricoles qu’il a lui‑même acquis auprès d’un producteur, le contrat de vente fait référence aux indicateurs mentionnés à l’avant‑dernier alinéa du II de l’article L. 631‑24 figurant dans le contrat d’achat conclu pour l’acquisition de ces produits.

« L’acheteur communique à l’autre partie, selon la fréquence convenue entre elles, l’évolution des indicateurs relatifs aux prix des produits agricoles et alimentaires constatés sur les marchés sur lesquels il opère.

« Art. L. 631‑24‑2. – I. – La conclusion de contrats de vente et accords‑cadres écrits mentionnés à l’article L. 631‑24 peut être rendue obligatoire par extension d’un accord interprofessionnel en application de l’article L. 632‑3, ou, en l’absence d’accord étendu, par un décret en Conseil d’État qui précise les produits ou catégories de produits concernés.

« Toutefois, les dispositions de l’alinéa précédent ne s’appliquent pas aux ventes directes au consommateur, aux cessions réalisées au bénéfice des organisations caritatives pour la préparation de repas destinés aux personnes défavorisées, aux cessions à prix ferme de produits agricoles sur les carreaux affectés aux producteurs situés au sein des marchés d’intérêt national définis à l’article L. 761‑1 du code de commerce ou sur d’autres marchés physiques de gros de produits agricoles, ainsi qu’aux entreprises dont le chiffre d’affaires est inférieur à un seuil défini par l’accord interprofessionnel ou le décret précités.

« Au cas où un accord est adopté et étendu après la publication d’un tel décret, l’application de celui‑ci est suspendue pendant la durée de l’accord.

« II. – L’accord interprofessionnel ou le décret en Conseil d’État mentionnés au I fixent la durée minimale du contrat de vente, qui ne peut excéder cinq ans, sauf renonciation expresse écrite du producteur. Ils peuvent prévoir que la durée minimale des contrats portant sur un produit dont le producteur a engagé la production depuis moins de cinq ans est augmentée dans la limite de deux ans.

« Les contrats portant sur un produit dont le producteur a engagé la production depuis moins de cinq ans ne peuvent être résiliés par l’acheteur avant le terme de la période minimale, sauf en cas d’inexécution par le producteur ou cas de force majeure. Ils fixent le préavis applicable au cas où le contrat ne serait pas renouvelé.

« Lorsqu’un acheteur a donné son accord à la cession, par le producteur, d’un contrat à un autre producteur engagé dans la production depuis moins de cinq ans, la durée restant à courir du contrat cédé, si elle est inférieure à la durée minimale fixée en application du premier alinéa du présent II, est prolongée pour atteindre cette durée.

« Est considéré comme un producteur ayant engagé une production depuis moins de cinq ans l’exploitant qui s’est installé ou a démarré une nouvelle production au cours de cette période, ainsi qu’une société agricole intégrant un nouvel associé répondant aux conditions fixées au présent alinéa et détenant au moins 10 % de son capital social.

« Un décret en Conseil d’État précise les produits considérés comme relevant de la même production pour l’application du présent article. Le décret ou l’accord interprofessionnel mentionné au I fixe le délai de mise en conformité des contrats en cours à la date de son intervention conclus avec un producteur ayant engagé la production depuis moins de cinq ans.

« Les dispositions relatives à la durée minimale du contrat prévues au premier alinéa du présent II ne sont applicables ni aux produits soumis à accises, ni aux raisins, moûts et vins dont ils résultent.

« Art. L. 631‑24‑3. – I. – Les articles L. 631‑24 à L. 631‑24‑2 sont d’ordre public.

« II. – Les dispositions mentionnées au I ne sont pas applicables aux relations des sociétés coopératives agricoles mentionnées à l’article L. 521‑1 avec leurs associés‑coopérateurs, non plus qu’aux relations entre les organisations de producteurs et associations d’organisations de producteurs bénéficiant d’un transfert de propriété des produits qu’elles commercialisent et les producteurs membres, si leurs statuts, leur règlement intérieur ou des règles ou décisions prévues par ces statuts ou en découlant comportent des dispositions produisant des effets similaires à ceux des clauses mentionnées à ces articles. Un exemplaire de ces documents est remis aux associés‑coopérateurs ou aux producteurs membres de l’organisation de producteurs ou de l’association d’organisations de producteurs en cause.

« Lorsque la coopérative, l’organisation de producteurs ou l’association d’organisations de producteurs commercialise des produits agricoles dont elle est propriétaire, ou des produits alimentaires comportant un ou plusieurs produits agricoles livrés par ses membres, le contrat de vente fait référence aux indicateurs utilisés pour la rémunération des producteurs de ces produits.

« III. – Les dispositions mentionnées au I ne sont pas applicables aux contrats passés avec les entreprises sucrières par les producteurs de betterave ou de canne à sucre. »

II. – Le code rural et de la pêche maritime est ainsi modifié :

« 1° Le dernier alinéa de l’article L. 665‑2 est supprimé ;

« 2° Au b de l’article L. 932‑5 et aux articles L. 952‑5 et L. 953‑3, les mots : « au I de l’article L. 631‑24 » sont remplacés par les mots : « aux 1° à 7° du II de l’article L. 631‑24. »

## Article 2

L’article L. 631‑25 du même code est remplacé par les dispositions suivantes :

« Art. L. 631‑25. – Est sanctionné par une amende administrative, dont le montant ne peut être supérieur à 75 000 € par an :

« 1° Le fait, pour un producteur, une organisation de producteurs, une association d’organisations de producteurs ou un acheteur de produits agricoles, de conclure un contrat écrit ou un accord‑cadre écrit ne comportant pas toutes les clauses mentionnées à l’article L. 631‑24 ou comprenant une délégation de facturation, en méconnaissance du IV de cet article ;

« 2° Le fait, pour un producteur ou un acheteur, de conclure un contrat ne respectant pas, en méconnaissance du I de l’article L. 631‑24, les stipulations d’un accord‑cadre ;

« 3° Le fait, pour un acheteur, de ne pas transmettre les informations prévues au dernier alinéa du III de l’article L. 631‑24 et à l’article L. 631‑24‑1 ;

« 4° Lorsque la conclusion de contrats de vente et d’accords‑cadres écrits a été rendue obligatoire dans les conditions prévues à l’article L. 631‑24‑2 :

« a) Le fait, pour une organisation de producteurs reconnue ou une association d’organisations de producteurs reconnue agissant comme mandataire de ses membres pour négocier la commercialisation des produits dont ils sont propriétaires, de ne pas proposer au premier acheteur de ces produits un accord‑cadre écrit ;

« b) Le fait, pour un producteur, de faire échec à la conclusion d’un contrat écrit en ne proposant pas de contrat à l’acheteur de ses produits ;

« c) Le fait, pour un acheteur, d’acheter des produits agricoles à un producteur sans avoir conclu de contrat écrit avec ce producteur ou sans avoir conclu d’accord‑cadre écrit avec l’organisation de producteurs ou l’association d’organisations de producteurs à laquelle il a donné mandat pour négocier la commercialisation de ses produits ou sans respecter les dispositions prises en application du II de l’article L. 631‑24‑2.

« Le montant de l’amende est proportionné à la gravité des faits constatés, notamment au nombre et au volume des ventes réalisées en infraction. Il peut être porté au double en cas de réitération du manquement dans un délai de deux ans à compter de la première commission des faits. L’autorité administrative compétente peut, en outre, ordonner la publication de la décision ou d’un extrait de celle‑ci.

« L’action de l’administration pour la sanction des manquements mentionnés ci‑dessus se prescrit par trois années révolues à compter du jour où le manquement a été commis si, dans ce délai, il n’a été fait aucun acte tendant à la recherche, à la constatation, ou à la sanction de ce manquement. »

## Article 3

L’article L. 631‑26 du même code est ainsi modifié :

1° La première phrase est remplacée par les dispositions suivantes : « Les manquements mentionnés à l’article L. 631‑25 sont constatés par des agents énumérés par décret en Conseil d’État. » ;

2° L’article est complété par un alinéa ainsi rédigé :

« Les agents mentionnés au premier alinéa peuvent, après une procédure contradictoire, enjoindre à l’auteur d’un des manquements énumérés à l’article L. 631‑25 de se conformer à ses obligations, en lui impartissant un délai raisonnable. Si, à l’issue de ce délai, le manquement persiste, l’agent le constate par un procès‑verbal qu’il transmet à l’autorité administrative compétente pour prononcer la sanction, dans les conditions prévues au deuxième alinéa. »

## Article 4

I. – L’article L. 631‑27 du même code est ainsi modifié :

1° À la première phrase du deuxième alinéa, les mots : « au I de » sont remplacés par le mot : « à » ;

2° Entre la première et la deuxième phrase du deuxième alinéa, il est inséré une phrase ainsi rédigée : « Il peut demander aux parties communication de tout élément nécessaire à la médiation. » ;

3° Après le deuxième alinéa, il est inséré un alinéa ainsi rédigé :

« Il recommande la suppression ou la modification des projets de contrats et accords‑cadres, ou des contrats et accords‑cadres, qu’il estime présenter un caractère abusif ou manifestement déséquilibré. » ;

4° Au quatrième alinéa, après les mots : « ou d’une organisation professionnelle ou syndicale » sont insérés les mots : « ou de sa propre initiative ».

II. – L’article L. 631‑28 du même code est remplacé par les dispositions suivantes :

« Art. L. 631‑28. – Tout litige entre professionnels relatif à l’exécution d’un contrat ou d’un accord‑cadre mentionnés à l’article L. 631‑24 ayant pour objet la vente de produits agricoles ou alimentaires doit, préalablement à toute saisine du juge, faire l’objet d’une procédure de médiation par le médiateur des relations commerciales agricoles, sauf si le contrat en dispose autrement ou en cas de recours à l’arbitrage.

« Le médiateur des relations commerciales agricoles fixe la durée de sa mission, qui ne peut excéder un mois. Le chapitre Ier du titre II de la loi n° 95‑125 du 8 février 1995 relative à l’organisation des juridictions et à la procédure civile, pénale et administrative est applicable à cette médiation. »

III. – À l’article L. 631‑29, les mots : « au III de l’article L. 631‑24 » et « au I de l’article L. 631‑24 » sont remplacés par les mots : « à l’article L. 631‑24‑2 ».

## Article 5

L’article L. 632‑2‑1 du même code est ainsi modifié :

1° Au deuxième alinéa, les mots : « des clauses types relatives aux modalités de détermination des prix, aux calendriers de livraison, aux durées de contrat, au principe de prix plancher, aux modalités de révision des conditions de vente en situation de fortes variations des cours des matières premières agricoles ainsi qu’à » sont remplacés par les mots : « des modèles de rédaction des clauses énumérées aux II et III de l’article L. 631‑24 et, le cas échéant, de la clause prévue à l’article L. 441‑8 du code de commerce, ainsi que de clauses relatives à » et les deux dernières phrases sont supprimées ;

2° Au troisième alinéa, après les mots : « la filière », sont insérés les mots : « , notamment les indicateurs mentionnés à l’avant‑dernier alinéa du II de l’article L. 631‑24. Elles peuvent formuler des recommandations sur la manière de les prendre en compte pour la détermination, la révision et la renégociation des prix ».

## Article 6

L’article L. 441‑8 du code de commerce est ainsi modifié :

1° Au premier alinéa, après les mots : « la vente des produits » sont insérés les mots : « agricoles et alimentaires », les mots : « la liste prévue au deuxième alinéa de l’article L. 442‑9, complétée, le cas échéant, » sont remplacés par les mots : « une liste prévue » et les mots : « matières premières agricoles et alimentaires » sont remplacés par les mots : « produits agricoles et alimentaires ou des coûts de l’énergie » ;

2° Le deuxième alinéa est remplacé par les dispositions suivantes :

« Cette clause, définie par les parties, précise les conditions de déclenchement de la renégociation et prend notamment en compte un ou plusieurs indicateurs des prix des produits agricoles ou alimentaires, le cas échéant définis par accords interprofessionnels. » ;

3° À la fin de la première phrase du troisième alinéa, le mot : « deux » est remplacé par le mot : « un » ;

4° Après le quatrième alinéa, il est inséré un alinéa ainsi rédigé :

« Si la renégociation de prix n’aboutit pas à un accord au terme du délai d’un mois prévu au troisième alinéa, et sauf recours à l’arbitrage, il est fait application des dispositions de l’article L. 631‑28 du code rural et de la pêche maritime sans que les stipulations du contrat puissent s’y opposer.

## Article 7

I. – L’article L. 694‑4 du code rural et de la pêche maritime est remplacé par les dispositions suivantes :

« Art. L. 694‑4. – I. – Pour l’application de l’article L. 631‑24‑2 à Saint‑Pierre‑et‑Miquelon :

« 1° Le premier alinéa du I est ainsi rédigé :

« I. – La conclusion ou la proposition de contrats de vente écrits peut être rendue obligatoire par un arrêté des ministres chargés de l’agriculture, de la consommation et des outre‑mer, qui précise les produits ou catégories de produits concernés. » ;

« 2° Le II est ainsi modifié :

« a) Le premier alinéa est remplacé par les dispositions suivantes :

« L’arrêté des ministres chargés de l’agriculture, de la consommation et des outre‑mer fixe la durée minimale du contrat de vente, qui ne peut excéder cinq ans, sauf renonciation expresse écrite du producteur. Il peut prévoir que la durée minimale des contrats portant sur un produit dont le producteur a engagé la production depuis moins de cinq ans est augmentée dans la limite de deux ans.» ;

« b) Au cinquième alinéa, les mots : “Le décret ou l’accord interprofessionnel” sont remplacés par les mots : “L’arrêté des ministres chargés de l’agriculture, de la consommation et des outre‑mer” ».

II. – À l’article L. 954‑3‑5 du code de commerce, les mots : « figurant sur la liste prévue au deuxième alinéa de l’article L. 442‑9 » sont remplacés par les mots : « figurant sur une liste prévue » et les mots : « fixée et » sont remplacés par le mot : « fixée ».

## Article 8

I. – Dans les conditions prévues à l’article 38 de la Constitution, le Gouvernement est habilité à prendre par ordonnance, dans un délai de six mois suivant la publication de la présente loi, toute mesure relevant du domaine de la loi tendant à modifier le code rural et de la pêche maritime afin :

1° D’adapter les dispositions de la section 1 du chapitre Ier et de la section 1 du chapitre IV du titre II du livre V relatives aux relations entre les sociétés coopératives agricoles et leurs associés coopérateurs, notamment pour définir les conditions de départ des associés coopérateurs, améliorer leur information, renforcer la transparence dans la redistribution des gains des coopératives à leurs associés coopérateurs et prévoir des modalités de contrôle et des sanctions permettant d’assurer l’application effective de ces dispositions ;

2° De recentrer les missions du Haut Conseil de la coopération agricole sur la mise en œuvre du droit coopératif et le contrôle de son respect et d’adapter les règles relatives à sa gouvernance et à sa composition ;

3° De modifier les conditions de nomination et d’intervention du médiateur de la coopération agricole pour assurer son indépendance et sa bonne coordination avec le médiateur des relations contractuelles agricoles ;

4° D’apporter au titre II du livre V les modifications éventuellement nécessaires pour assurer le respect de la hiérarchie des normes, la cohérence rédactionnelle des textes, harmoniser l’état du droit, remédier aux éventuelles erreurs et abroger les dispositions devenues sans objet.

II. – Un projet de loi de ratification est déposé devant le Parlement dans un délai de trois mois à compter de la publication de chaque ordonnance prévue par le présent article.

## Article 9

Dans les conditions prévues à l’article 38 de la Constitution, le Gouvernement est autorisé à prendre par ordonnance, dans un délai de six mois à compter de la publication de la présente loi, toute mesure relevant du domaine de la loi et ressortissant au code de commerce nécessaire pour prévoir sur une durée de deux ans :

1° D’affecter le prix d’achat effectif défini au deuxième alinéa de l’article L. 442‑2 du code de commerce d’un coefficient égal à 1,1 pour les denrées alimentaires revendues en l’état au consommateur ;

2° D’encadrer en valeur et en volume les opérations promotionnelles portant sur la vente au consommateur de denrées alimentaires et de définir les sanctions permettant d’assurer l’effectivité de ces dispositions.

## Article 10

I. – Dans les conditions prévues à l’article 38 de la Constitution, le Gouvernement est autorisé à prendre par voie d’ordonnance, dans un délai de neuf mois à compter de la publication de la présente loi, toute mesure relevant du domaine de la loi nécessaire pour modifier le titre IV du livre IV du code de commerce afin de :

1° Réorganiser ce titre et clarifier ses dispositions, notamment en supprimant les dispositions devenues sans objet et en renvoyant le cas échéant à d’autres codes ;

2° Clarifier les règles de facturation, en les harmonisant avec les dispositions du code général des impôts et modifier en conséquence les sanctions relatives aux manquements à ces règles ;

3° Préciser les dispositions relatives aux conditions générales de vente et mettre en cohérence les dispositions relatives aux produits agricoles et alimentaires, notamment en ce qui concerne les références applicables aux critères et modalités de détermination des prix, avec les dispositions du code rural et de la pêche maritime ;

4° Simplifier les dispositions relatives aux conventions conclues entre les fournisseurs et les distributeurs ou les prestataires de service et entre les fournisseurs et les grossistes et préciser le régime des avenants à ces conventions ;

5° Simplifier et préciser les définitions des pratiques mentionnées à l’article L. 442‑6, notamment en ce qui concerne la rupture brutale des relations commerciales et les voies d’actions en justice ;

6° Élargir à l’article L. 442‑9 le champ d’application de l’action en responsabilité.

II. – Dans les conditions prévues à l’article 38 de la Constitution, le Gouvernement est autorisé à prendre par voie d’ordonnance, dans un délai de neuf mois à compter de la publication de la présente loi, toute mesure relevant du domaine de la loi nécessaire pour mettre en cohérence les dispositions de tous codes avec celles prises par voie d’ordonnance en application du I.

III. – Un projet de loi de ratification est déposé devant le Parlement dans un délai de trois mois à compter de la publication de chaque ordonnance prévue par le présent article.

# TITRE II

# Mesures en faveur d’une alimentation saine,de qualitÉ et durable

## Article 11

Après l’article L. 230‑5 du code rural et de la pêche maritime, il est inséré un article L. 230‑5‑1 ainsi rédigé :

« Art. L. 230‑5‑1. – Au plus tard le 1er janvier 2022, les personnes morales de droit public incluent, dans la composition des repas servis dans les restaurants collectifs dont elles ont la charge, une part significative de produits acquis en prenant en compte le coût du cycle de vie du produit, ou issus de l’agriculture biologique, ou bénéficiant d’un des autres signes ou mentions prévus par l’article L. 640‑2 du code rural et de la pêche maritime ou satisfaisant de manière équivalente aux exigences définies par ces signes ou mentions.

« Un décret en Conseil d’État précise les modalités d’application du présent article, notamment les conditions de l’application progressive et les modalités du suivi de sa mise en œuvre ainsi que le pourcentage de produits acquis devant entrer dans la composition des repas. »

## Article 12

I. – L’article L. 230‑6 du code rural et de la pêche maritime est abrogé.

II. – Le titre VI du livre II du code de l’action sociale et des familles est complété par un chapitre VI ainsi rédigé :

(3) « Chapitre VI

(4) « Lutte contre la précarité alimentaire

« Art. L. 266‑1. – L’aide alimentaire a pour objet la fourniture de denrées alimentaires aux personnes en situation de vulnérabilité économique ou sociale, assortie de la proposition d’un accompagnement.

« Seules des personnes morales de droit public ou des personnes morales de droit privé habilitées par l’autorité administrative, pour une durée et selon des conditions et modalités fixées par décret en Conseil d’État, peuvent recevoir des contributions publiques destinées à la mise en œuvre de l’aide alimentaire.

« Ces conditions doivent notamment permettre de garantir la fourniture de l’aide alimentaire sur une partie suffisante du territoire et sa distribution auprès de tous les bénéficiaires potentiels, d’assurer la traçabilité physique et comptable des denrées et de respecter de bonnes pratiques d’hygiène relatives au transport, au stockage et à la mise à disposition des denrées.

« Sont également déterminées par décret en Conseil d’État les modalités de collecte et de transmission à l’autorité administrative, par les personnes morales habilitées en application du deuxième alinéa, des données portant sur leur activité, sur les denrées distribuées et, une fois rendues anonymes, sur les bénéficiaires de l’aide alimentaire. La collecte et la transmission de ces données s’effectuent dans le respect de la loi n° 78‑17 du 6 janvier 1978 relative à l’informatique, aux fichiers et aux libertés. »

III. – À l’article L. 541‑15‑5 du code de l’environnement, les mots : « association caritative habilitée en application de l’article L. 230‑6 du code rural et de la pêche maritime » sont remplacés par les mots : « personne morale habilitée en application de l’article L. 266‑1 du code de l’action sociale et des familles ».

## Article 13

I. – Le premier alinéa de l’article 2‑13 du code de procédure pénale est complété par les mots : « et par le code rural et de la pêche maritime ».

II. – Le premier alinéa de l’article L. 215‑11 du code rural et de la pêche maritime est ainsi modifié :

1° Les mots : « de six mois » sont remplacés par les mots : « d’un an » ;

2° Le montant : « 7 500 € » est remplacé par le montant : « 15 000 € » ;

3° Après le mot : « refuge », sont insérés les mots : « , un établissement d’abattage ou de transport d’animaux vivants ».

## Article 14

Au chapitre III du titre V du livre II du code rural et de la pêche maritime, il est inséré une section 4 bis ainsi rédigée :

(2) « Section 4 bis

(3) « Pratiques commerciales prohibées

« Art. L. 253‑5‑1. – À l’occasion de la vente de produits phytopharmaceutiques définis à l’article L. 253‑1, les remises, rabais, ristournes, la différenciation des conditions générales et particulières de vente au sens du I de l’article L. 441‑6 du code de commerce ou la remise d’unités gratuites et toutes pratiques équivalentes sont interdits. Toute pratique commerciale visant à contourner, directement ou indirectement, cette interdiction par l’attribution de remises, rabais ou ristournes sur une autre gamme de produits qui serait liée à l’achat de ces produits est prohibée. Les dispositions du présent article ne s’appliquent pas aux produits de biocontrôle définis à l’article L. 253‑6, ni aux substances de base au sens de l’article 23 du règlement (CE) n° 1107/2009 du Parlement européen et du Conseil du 21 octobre 2009 concernant la mise sur le marché des produits phytopharmaceutiques et abrogeant les directives 79/117/CEE et 91/414/CEE du Conseil, ni aux produits à faible risque au sens du même règlement.

« Art. L. 253‑5‑2. – I. – Tout manquement aux interdictions prévues à l’article L. 253‑5‑1 est passible d’une amende administrative dont le montant ne peut excéder 15 000 € pour une personne physique et 75 000 € pour une personne morale.

« II. – Le montant de l’amende mentionnée au I est doublé en cas de réitération du manquement dans un délai de deux ans à compter de la date à laquelle la première décision de sanction est devenue définitive.

« Cette amende peut être assortie d’une astreinte journalière d’un montant maximal de 1 000 € lorsque l’auteur de l’infraction n’a pas mis fin au manquement à l’issue d’un délai fixé par une mise en demeure.

« III. – L’autorité administrative compétente avise préalablement l’auteur du manquement des faits relevés à son encontre des dispositions qu’il a enfreintes et des sanctions qu’il encourt. Elle lui fait connaître le délai dont il dispose pour faire valoir ses observations écrites et, le cas échéant, les modalités selon lesquelles il peut être entendu s’il en fait la demande. Elle l’informe de son droit à être assisté du conseil de son choix.

« La décision de sanction ne peut être prise plus d’un an à compter de la constatation des faits. Elle peut faire l’objet d’un recours de pleine juridiction devant la juridiction administrative. »

## Article 15

I. – Dans les conditions prévues à l’article 38 de la Constitution, le Gouvernement est habilité à prendre par ordonnance, dans un délai de six mois suivant la publication de la présente loi, toute mesure relevant du domaine de la loi tendant à modifier le code rural et de la pêche maritime et le code de la consommation afin de :

1° Rendre l’exercice des activités mentionnées aux 1° et 2° du II de l’article L. 254‑1 du code rural et de la pêche maritime incompatible avec celui de l’activité de conseil à l’utilisation de produits phytopharmaceutiques autre que celle portant sur les informations relatives à l’utilisation, aux risques et à la sécurité d’emploi des produits cédés et modifier le régime applicable aux activités de conseil et de vente de ces produits, notamment en imposant une séparation capitalistique des structures exerçant ces activités ;

2° Réformer le régime d’expérimentation des certificats d’économie de produits phytopharmaceutiques :

– en fixant des objectifs à atteindre à une date antérieure à 2021 ;

– en le transformant en régime permanent à périodes successives, avec les adaptations nécessaires à son bon fonctionnement ;

– en prévoyant son application outre‑mer ;

3° Confier aux agents mentionnés à l’article L. 205‑1 du code rural et de la pêche maritime et aux agents mentionnés à l’article L. 511‑3 du code de la consommation les pouvoirs dont disposent, en application de l’article L. 172‑8 du code de l’environnement, les fonctionnaires et agents mentionnés à l’article L. 172‑4 de ce code ;

4° Confier aux agents mentionnés à l’article L. 205‑1 du code rural et de la pêche maritime les pouvoirs d’enquête dont disposent les agents habilités par le code de la consommation, prévus aux articles L. 512‑7, L. 512‑10 et L. 512‑16 de ce code.

II. – Dans les conditions prévues à l’article 38 de la Constitution, le Gouvernement est habilité à prendre par ordonnance, dans un délai de douze mois suivant la publication de la présente loi, toute mesure relevant du domaine de la loi afin de :

1° Modifier la portée de l’obligation fixée à l’article L. 541‑15‑3 du code de l’environnement pour, d’une part, l’étendre à l’ensemble des opérateurs de la restauration collective et, d’autre part, leur imposer la réalisation d’un diagnostic préalable à la démarche de lutte contre le gaspillage alimentaire ;

2° Prévoir les conditions dans lesquelles les obligations fixées aux articles L. 541‑15‑5 et L. 541‑15‑6 du même code sont étendues à certains opérateurs du secteur agro‑alimentaire et de la restauration collective ;

3° Imposer à certains opérateurs de rendre publics leurs engagements en faveur de la lutte contre le gaspillage alimentaire ;

4° Apporter au titre préliminaire et au titre V du livre II du code rural et de la pêche maritime ainsi qu’au titre IV du livre V du code de l’environnement les modifications éventuellement nécessaires pour assurer le respect de la hiérarchie des normes, la cohérence rédactionnelle des textes, harmoniser l’état du droit, remédier aux éventuelles erreurs et abroger les dispositions devenues sans objet.

III. – Un projet de loi de ratification est déposé devant le Parlement dans un délai de trois mois à compter de la publication de chaque ordonnance prévue par le présent article.

# TITRE III

# DISPOSITIONS TRANSITOIRES ET FINALES

## Article 16

I. – Les articles 1er et 2 entrent en vigueur le premier jour du troisième mois suivant la publication de la présente loi.

Dans les secteurs où la conclusion de contrats écrits est obligatoire :

– les accords‑cadres conclus avant la date d’entrée en vigueur de la loi sont mis en conformité avec l’article L. 631‑24 du code rural et de la pêche maritime, dans sa rédaction résultant de la présente loi, au plus tard le 1er septembre 2018 ou, si cette date est postérieure, au plus tard un mois après la date d’entrée en vigueur de la présente loi ; les organisations de producteurs ou associations d’organisations de producteurs concernées proposent aux acheteurs un avenant à cet effet ;

– les contrats conclus avant cette date et se poursuivant au‑delà du 1er octobre 2018 sont mis en conformité avec l’article L. 631‑24 du code rural et de la pêche maritime, dans sa rédaction résultant de la présente loi, au plus tard le 1er octobre 2018 ou, si cette date est postérieure, au plus tard deux mois après la date d’entrée en vigueur de la présente loi ; les producteurs concernés proposent aux acheteurs un avenant à cet effet, ou leur demandent par écrit de leur proposer cet avenant.

Dans les autres secteurs, les contrats en cours à la date d’entrée en vigueur de la présente loi doivent être mis en conformité avec les dispositions de l’article L. 631‑24 du code rural et de la pêche maritime, dans sa rédaction issue de la présente loi, lors de leur prochain renouvellement et au plus tard dans un délai d’un an à compter de la publication de la présente loi.

II. – Les dispositions de l’article 3 entrent en vigueur à la date prévue au premier alinéa du I, ou, si cette date est postérieure, à compter de la date de publication du décret codifiant dans la partie réglementaire du code rural et de la pêche maritime la liste des agents habilités à constater les manquements aux dispositions de la section 2 du chapitre Ier du titre III du livre VI du code rural et de la pêche maritime.

III. – Les dispositions de l’article 4 ne sont pas applicables aux procédures de médiation en cours à la date de publication de la présente loi.

IV. ‑ Les renégociations de prix, ainsi que les procédures de médiation et instances juridictionnelles qui sont en cours à la date de publication de la présente loi restent soumises à l’article L. 441‑8 du code de commerce dans sa rédaction antérieure.

V. – Les dispositions de l’article 14 s’appliquent aux contrats conclus ou renouvelés à compter de la date prévue au premier alinéa du I.

## Article 17

Au I de l’article L. 950‑1 du code de commerce, la ligne :

«

Articles L. 441‑8 et L. 441‑9

L’ordonnance n° 2014‑487 du 15 mai 2014

»

est remplacée par les lignes :

«

Article L. 441‑8

La loi n°       du

Article L. 441‑9

L’ordonnance n° 2014‑487 du 15 mai 2014

».